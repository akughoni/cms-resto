<?php

class Base_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  public function get_all_data($table)
  {
    return $this->db->get($table);
  }

  public function get_data_by($table, $field, $data)
  {
    return $this->db->get_where($table, [$field => $data]);
  }

  public function insert_data($table, $data)
  {
    $this->db->insert($table, $data);
  }

  public function delete_data($table, $field, $data)
  {
    $this->db->delete($table, [$field => $data]);
  }

  public function update_data_text($table, $data)
  {
    $this->db->update($table, $data);
  }

  public function update_data_by($table, $data, $field, $key)
  {
    $this->db->update($table, $data, [$field => $key]);
  }
}
