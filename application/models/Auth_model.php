<?php

class Auth_model extends CI_Model
{
  public function register($data)
  {
    $this->db->insert('user', $data);
  }

  public function user_check($data)
  {
    return $this->db->get_where('user', array('username' => $data));
  }
}