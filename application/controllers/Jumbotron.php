<?php
require_once 'Base.php';

class Jumbotron extends Base
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('base_model');
    $this->load->library('form_validation');
  }
  public function index()
  {
    $query = $this->base_model->get_all_data('jumbotron')->row();
    $data['data']['item'] = $query;
    $data['content_page'] = 'jumbo/index';
    $this->template($data);
  }

  public function update_text()
  {
    $query = $this->base_model->get_all_data('jumbotron')->row();
    $data['data']['item'] = $query;
    $data['content_page'] = 'jumbo/update_text';
    $this->template($data);
  }

  public function store_update()
  {
    $this->form_validation->set_rules('title', 'Title', 'required');
    $this->form_validation->set_rules('desc_1', 'Subtitle', 'required');
    $this->form_validation->set_rules('desc_2', 'Description', 'required');

    if ($this->form_validation->run()) {
      $text['title'] = $this->input->post('title');
      $text['desc_1'] = $this->input->post('desc_1');
      $text['desc_2'] = $this->input->post('desc_2');
      $this->base_model->update_data_text('jumbotron', $text);
      redirect('jumbotron/');
    } else {
      $query = $this->base_model->get_all_data('jumbotron')->row();
      $data['data']['item'] = $query;
      $data['content_page'] = 'jumbo/update_text';
      $this->template($data);
    }
  }

  public function update_bg()
  {
    $data['content_page'] = 'jumbo/update_bg';
    $this->template($data);
  }

  public function store_bg()
  {
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['upload_path'] = './dist/img/bg_jumbotron/';
    //call library upload and set config
    $this->load->library('upload', $config);

    //check return uploaded photo
    if (!$this->upload->do_upload('background')) {
      $message = $this->upload->display_errors();
      $this->session->set_flashdata('message', $message);
      $this->session->set_flashdata('alert', 'alert-warning');
      redirect('jumbotron/');
    } else {
      $insertData['background'] = $this->upload->data('file_name');
      $query = $this->base_model->get_all_data('jumbotron')->row();
      unlink('./dist/img/bg_jumbotron/' . $query->background);
      $this->base_model->update_data_text('jumbotron', $insertData);
      $this->session->set_flashdata('message', 'Update Background Success....');
      $this->session->set_flashdata('alert', 'alert-success');
      redirect('jumbotron/');
    }
  }
}
