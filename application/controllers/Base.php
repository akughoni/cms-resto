<?php

class Base extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('auth_model');
    $this->load->model('base_model');
    $this->load->library('form_validation');
  }
  public function login_check()
  {
    $test = $this->session->has_userdata('login');
    return $test;
  }

  public function template($data = null)
  {
    $this->load->view('template', $data);
  }

  public function Auth_template($data = null)
  {
    $this->load->view('Auth/template', $data);
  }
  public function check_username($data = null)
  {
    $query = $this->auth_model->user_check($data)->row();
    if (isset($query)) {
      return true;
    } else {
      return false;
    }
  }
}
