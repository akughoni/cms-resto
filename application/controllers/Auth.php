<?php 
require_once 'Base.php';

class Auth extends Base
{
  public $data=array(
    'message' => null,
    'content_page' => null
  );
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('auth_model');
    $this->load->library('form_validation');
  }

  public function auth_login()
  {
    $this->data['content_page']='Auth/login';
    $this->auth_template($this->data);
  }

  public function store_login()
  {
    $this->form_validation->set_rules('username', 'Username','required|min_length[4]');
    $this->form_validation->set_rules('password','Password','required|min_length[8]');
    if($this->form_validation->run() == false)
    {
      $this->data['content_page']='Auth/login';
      $this->auth_template($this->data);
    }
    else
    {
      $user['username'] = $this->input->post('username');
      $user_check = $this->check_username($user['username']); //fungsi cek apakah username ada ? tidak!
      if($user_check==false)
      {
        $this->data['content_page'] = 'Auth/login';
        $this->data['message'] = 'Username Not Found';
        $this->auth_template($this->data);
      }
      else
      {
        $password = $this->input->post('password');
        $query = $this->auth_model->user_check($user['username'])->row(); 
        $hash = $query->password;
        // $pass_check = password_verify($pass , $hash);
        // var_dump($password);
        if(password_verify($password,$hash))
        { 
          $auth=['login' => $user['username']];
          $this->session->set_userdata($auth);
          // $this->data['content_page'] = 'home';
          // $this->auth_template($this->data);
          redirect('/');
        }
        else
        {
          $this->data['content_page'] = 'Auth/login';
          $this->data['message'] = 'Password is wrong!...';
          $this->auth_template($this->data);
        }
      }
    }
  }

  public function auth_register()
  {
    $this->data['content_page']='Auth/register';
    $this->auth_template($this->data);
  }

  public function store_register()
  {
    $this->form_validation->set_rules('username', 'Username','required|min_length[4]');
    $this->form_validation->set_rules('email','Email', 'required');
    $this->form_validation->set_rules('password','Password','required|min_length[8]');
    if($this->form_validation->run()==false)
    {
      $this->data['content_page'] = 'Auth/register';
      $this->auth_template($this->data);
    }else{
      $user['username'] = $this->input->post('username');
      $user_check = $this->check_username($user['username']);
      if($user_check)
      {
        $this->data['content_page'] = 'Auth/register';
        $this->data['message'] = 'Username Already axis!...';
        $this->auth_template($this->data);
      }else
      {
        $user['email'] = $this->input->post('email');
        $pass = $this->input->post('password');
        $user['password'] = password_hash($pass, PASSWORD_BCRYPT);

        $this->auth_model->register($user);
        $this->data['content_page'] = 'Auth/login';
        $this->data['message'] = 'User Registrasion success!....';
        $this->auth_template($this->data);
      }
    }
  }

  public function forgot_password()
  {

  }

  public function auht_logout()
  {
    $this->session->unset_userdata('login');
    redirect('auth/auth_login');
  }
}