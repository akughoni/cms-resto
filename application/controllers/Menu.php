<?php

require_once 'Base.php';

class Menu extends Base
{
  // public $path_upload = 'dist/user_upload';

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('form');
    $this->load->helper('file');
    $this->load->model('base_model');
  }

  public function index()
  {
    $data['content_page'] = 'menu/index';
    $query = $this->base_model->get_all_data('menu');
    $data['data']['menu_list'] = $query->result();
    $this->template($data);
  }

  public function insert_menu()
  {
    $query = $this->base_model->get_all_data('category')->result_array();
    $data['data']['category'] = $query;
    $data['content_page'] = 'menu/insert_menu';
    $this->template($data);
  }

  public function store_insert()
  {
    $insertData['name'] = $this->input->post('name');
    $insertData['slug'] = slug($this->input->post('name'));
    $insertData['id_category'] = $this->input->post('kd_cat');
    $insertData['detail'] = $this->input->post('detail');
    //config rule upload
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['upload_path'] = './dist/user_upload/';
    //call library upload and set config
    $this->load->library('upload', $config);

    //check return uploaded photo
    if (!$this->upload->do_upload('photo')) {
      $data['data']['message'] = $this->upload->display_errors();
      $data['content_page'] = 'menu/insert_menu';
      $this->template($data);
    } else {
      $insertData['photo'] = $this->upload->data('file_name');
      $this->base_model->insert_data('menu', $insertData);
      $this->session->set_flashdata('message', 'Add Menu Success....');
      $this->session->set_flashdata('alert', 'alert-success');
      redirect('menu/');
    }
  }

  public function delete_menu()
  {
    $slug = $this->uri->segment(3);
    $query = $this->base_model->get_data_by('menu', 'slug', $slug)->row();
    $name_file = $query->photo;
    $del_status = unlink('./dist/user_upload/' . $name_file);
    if ($del_status) {
      $this->base_model->delete_data('menu', 'slug', $slug);
      $this->session->set_flashdata('message', 'Delete Failed....');
      $this->session->set_flashdata('alert', 'alert-success');
      redirect('menu/');
    } else {
      $this->session->set_flashdata('message', 'Delete Failed....');
      $this->session->set_flashdata('alert', 'alert-warning');
      redirect('menu/');
    }
  }
}
