<?php

require_once 'Base.php';

class Favorite_item extends Base
{
  public function __construct()
  {
    parent::__construct();
    $this->load->helper('form');
    $this->load->helper('file');
  }

  public function index()
  {
    $data['data']['list_item'] = $this->base_model->get_all_data('item_fav')->result();
    $data['content_page'] = 'favorite/list_item';
    $this->template($data);
  }

  public function insert_item()
  {
    $data['content_page'] = 'favorite/insert_item';
    $this->template($data);
  }

  public function store_insert()
  {
    $this->form_validation->set_rules('title', 'Title', 'required');
    $this->form_validation->set_rules('desc', 'Descriptions', 'required');

    if ($this->form_validation->run()) {
      $insertData['title'] = $this->input->post('title');
      $insertData['slug'] = slug($this->input->post('title'));
      $insertData['desc'] = $this->input->post('desc');
      $config['allowed_types'] = 'gif|jpg|png|jpeg'; //config rule upload
      $config['upload_path'] = './dist/img/bg_favorite/';
      $this->load->library('upload', $config); //call library upload and set config
      if (!$this->upload->do_upload('photo')) { //check return uploaded photo
        $message = $this->upload->display_errors();
        $this->session->set_flashdata('message', $message);
        $this->session->set_flashdata('alert', 'alert-danger');
        $data['content_page'] = 'favorite/insert_item';
        $this->template($data);
      } else {
        $insertData['bg'] = $this->upload->data('file_name');
        $this->base_model->insert_data('item_fav', $insertData);
        $this->session->set_flashdata('message', 'Add Favorite Item Success....');
        $this->session->set_flashdata('alert', 'alert-success');
        redirect('favorite_item/');
      }
    } else {
      $data['content_page'] = 'favorite/insert_item';
      $this->template($data);
    }
  }

  public function update_text()
  {
    $data['data']['item'] = $this->base_model->get_data_by('item_fav', 'id', $this->uri->segment(3))->row();
    $data['content_page'] = 'favorite/update_text';
    $this->template($data);
  }

  public function store_update()
  {
    $this->form_validation->set_rules('title', 'Title', 'required');
    $this->form_validation->set_rules('desc', 'Description', 'required');
    $key = $this->uri->segment(3);

    if ($this->form_validation->run()) {
      $updateData['title'] = $this->input->post('title');
      $updateData['slug'] = slug($this->input->post('title'));
      $updateData['desc'] = $this->input->post('desc');
      $this->base_model->update_data_by('item_fav', $updateData, 'id', $key);
      redirect('favorite_item/');
    } else {
      $this->session->set_flashdata('message', 'Update Favorite Item Failed...');
      $this->session->set_flashdata('alert', 'alert-danger');
      $data['data']['item'] = $this->base_model->get_data_by('item_fav', 'id', $this->uri->segment(3))->row();
      $data['content_page'] = 'favorite/update_text';
      $this->template($data);
    }
  }

  public function update_bg()
  {
    $data['data']['item'] = $this->base_model->get_data_by('item_fav', 'id', $this->uri->segment(3))->row();
    $data['content_page'] = 'favorite/update_bg';
    $this->template($data);
  }

  public function store_bg()
  {
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['upload_path'] = './dist/img/bg_favorite/';
    //call library upload and set config
    $this->load->library('upload', $config);

    //check return uploaded photo
    if (!$this->upload->do_upload('bg')) {
      $message = $this->upload->display_errors();
      $this->session->set_flashdata('message', $message);
      $this->session->set_flashdata('alert', 'alert-warning');
      $data['data']['item'] = $this->base_model->get_data_by('item_fav', 'id', $this->uri->segment(3))->row();
      $data['content_page'] = 'favorite/update_bg';
      $this->template($data);
    } else {
      $insertData['bg'] = $this->upload->data('file_name');
      $query = $this->base_model->get_data_by('item_fav', 'id', $this->uri->segment(3))->row();
      unlink('./dist/img/bg_favorite/' . $query->bg);
      $this->base_model->update_data_by('item_fav', $insertData, 'id', $query->id);
      $this->session->set_flashdata('message', 'Update Background Success....');
      $this->session->set_flashdata('alert', 'alert-success');
      redirect('favorite_item/');
    }
  }

  public function delete_item()
  {
    $id = $this->uri->segment(3);
    $query = $this->base_model->get_data_by('item_fav', 'id', $id)->row();
    $name_file = $query->bg;
    $del_status = unlink('./dist/img/bg_favorite/' . $name_file);
    if ($del_status) {
      $this->base_model->delete_data('item_fav', 'id', $id);
      $this->session->set_flashdata('message', 'Delete Item Success....');
      $this->session->set_flashdata('alert', 'alert-success');
      redirect('favorite_item/');
    } else {
      $this->session->set_flashdata('message', 'Delete Failed....');
      $this->session->set_flashdata('alert', 'alert-warning');
      redirect('favorite_item/');
    }
  }
}
