<?php
require_once 'Base.php';

class Motto extends Base
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('base_model');
    $this->load->library('form_validation');
  }

  public function index()
  {
    $query = $this->base_model->get_all_data('motto')->row();
    $data['data']['item'] = $query;
    $data['content_page'] = 'motto/index';
    $this->template($data);
  }

  public function update_desc()
  {
    $this->form_validation->set_rules('desc', 'Descriptions', 'required');
    if ($this->form_validation->run()) {
      $text['desc'] = $this->input->post('desc');
      $this->base_model->update_data_text('motto', $text);
      $this->session->set_flashdata('message', 'Update Description Success...');
      $this->session->set_flashdata('alert', 'alert-success');
      redirect('motto/');
    } else {
      $this->session->set_flashdata('message', 'Update Description Failed...');
      $this->session->set_flashdata('error', form_error('desc'));
      $this->session->set_flashdata('alert', 'alert-warning');
      redirect('motto/');
    }
  }

  public function store_bg()
  {
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['upload_path'] = './dist/img/bg_motto/';
    $this->load->library('upload', $config); //call library upload and set config
    if (!$this->upload->do_upload('background')) {     //check return uploaded photo
      $message = $this->upload->display_errors();
      $this->session->set_flashdata('message', $message);
      $this->session->set_flashdata('alert', 'alert-warning');
      redirect('motto/');
    } else {
      $query = $this->base_model->get_all_data('motto')->row();
      $status = unlink('./dist/img/bg_motto/' . $query->bg);
      $insertData['bg'] = $this->upload->data('file_name');
      $this->base_model->update_data_text('motto', $insertData);
      $this->session->set_flashdata('message', 'Update Background Success....');
      $this->session->set_flashdata('alert', 'alert-success');
      redirect('motto/');
    }
  }
}
