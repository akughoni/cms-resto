<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once 'Base.php';

class Dashboard extends Base
{
  public function __construct()
  {
    parent::__construct();
    if ($this->login_check() == false) {
      redirect('/auth/auth_login');
    }
  }
  public function index()
  {
    $data['content_page'] = 'home';
    $this->template($data);
  }
}
