<?php
require_once 'Base.php';

class Favorite extends Base
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('base_model');
    $this->load->library('form_validation');
  }

  public function index()
  {
    $query = $this->base_model->get_all_data('favorite')->row();
    $data['data']['item'] = $query;
    $data['content_page'] = 'favorite/index';
    $this->template($data);
  }

  public function update_title()
  {
    $this->form_validation->set_rules('title', 'Title', 'required');
    if ($this->form_validation->run()) {
      $text['title'] = $this->input->post('title');
      $this->base_model->update_data_text('favorite', $text);
      $this->session->set_flashdata('message', 'Update Title Success...');
      $this->session->set_flashdata('alert', 'alert-success');
      redirect('favorite/');
    } else {
      $this->session->set_flashdata('message', 'Update Title Failed...');
      $this->session->set_flashdata('alert', 'alert-warning');
      $this->session->set_flashdata('error', form_error('title'));
      redirect('favorite/');
    }
  }

  public function store_bg()
  {
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['upload_path'] = './dist/img/bg_favorite/';
    $this->load->library('upload', $config); //call library upload and set config
    if (!$this->upload->do_upload('background')) {     //check return uploaded photo
      $message = $this->upload->display_errors();
      $this->session->set_flashdata('message', $message);
      $this->session->set_flashdata('alert', 'alert-warning');
      redirect('favorite/');
    } else {
      $query = $this->base_model->get_all_data('favorite')->row();
      unlink('./dist/img/bg_favorite/' . $query->bg);
      $insertData['bg'] = $this->upload->data('file_name');
      $this->base_model->update_data_text('favorite', $insertData);
      $this->session->set_flashdata('message', 'Update Background Success....');
      $this->session->set_flashdata('alert', 'alert-success');
      redirect('favorite/');
    }
  }
}
