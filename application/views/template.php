<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Admin Login</title>
  <link href="<?= base_url() ?>dist/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="<?= base_url() ?>dist/css/sb-admin-2.min.css" rel="stylesheet">
</head>

<body>
  <div id="wrapper">
    <?php $this->load->view('component/sidebar'); ?>
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <?php $this->load->view('component/navbar'); ?>
        <div class="container">
          <?php
          if (isset($data)) {
            $this->load->view($content_page, $data);
          } else {
            $this->load->view($content_page);
          }
          ?>
        </div>
      </div>
    </div>

  </div>

  <script src="<?= base_url() ?>dist/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url() ?>dist/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url() ?>dist/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="<?= base_url() ?>dist/js/sb-admin-2.min.js"></script>
  <script src="<?= base_url() ?>dist/vendor/chart.js/Chart.min.js"></script>
  <script src="<?= base_url() ?>dist/js/demo/chart-area-demo.js"></script>
  <script src="<?= base_url() ?>dist/js/demo/chart-pie-demo.js"></script>
</body>

</html>