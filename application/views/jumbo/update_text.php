<div class="card p-2 text-center">
  <h3>Jumbotron Preferences</h3>
</div>
<div class="card mt-3 p-5">
  <form action="<?= base_url() ?>/jumbotron/store_update" method="post">
    <div class="row form-group">
      <label class="col-2 form-label" for="title">Title</label>
      <div class="col-10">
        <input name="title" value="<?= $item->title ?>" class="col form-control" type="text" id="title">
        <span class="form-text text-danger"><?= form_error('title') ?></span>
      </div>
    </div>
    <div class="row form-group">
      <label class="col-2 form-label" for="subtitle">Subtitle</label>
      <div class="col-10">
        <input name="desc_1" value="<?= $item->desc_1 ?>" class="col form-control" type="text" id="subtitle">
        <span class="form-text text-danger"><?= form_error('desc_1') ?></span>
      </div>
    </div>
    <div class="row form-group">
      <label class="col-2 form-label" for="desc">Descriptions</label>
      <div class="col-10">
        <textarea name="desc_2" class="col form-control" rows="6" id="desc"><?= $item->desc_2 ?></textarea>
        <span class="form-text text-danger"><?= form_error('desc_2') ?></span>
      </div>
    </div>
    <div class="row text-right">
      <div class="col">
        <button type="submit" class="btn btn-primary btn-icon-split">
          <span class="text col">Save Changes</span>
        </button>
      </div>
    </div>
  </form>
</div>