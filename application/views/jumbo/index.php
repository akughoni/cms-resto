<?php if ($this->session->flashdata('message')) { ?>
  <div class="alert <?= $this->session->flashdata('alert') ?>">
    <span><?= $this->session->flashdata('message') ?></span>
  </div>
<?php } ?>
<div class="card p-2 text-center">
  <h3>Jumbotron Preferences</h3>
</div>
<div class="card mt-3 p-5">
  <div class="row">
    <div class="col-7">
      <div class="row form-group">
        <label class="col-2 form-label" for="title">Title</label>
        <input name="title" value="<?= $item->title ?>" class="form-control" type="text" name="title" id="title">
      </div>
      <div class="row form-group">
        <label class="col-2 form-label" for="title">Subtitle</label>
        <input name="desc_1" value="<?= $item->desc_1 ?>" class="form-control" type="text" name="title" id="title">
      </div>
      <div class="row form-group">
        <label class="col form-label" for="title">Descriptions</label>
        <textarea name="desc_2" class="form-control" rows="6"><?= $item->desc_2 ?></textarea>
      </div>
      <div class="row text-right">
        <div class="col">
          <a href="<?= base_url() ?>jumbotron/update_text" class="btn btn-primary btn-icon-split">
            <span class="icon text-white-50">
              <i class="fas fa-pen"></i>
            </span>
            <span class="text col">Change Text</span>
          </a>
        </div>
      </div>
    </div>
    <div class="offset-1 col-4">
      <div class="row">
        <label class="col-12 form-label" for="title">Background Image</label>
        <div class="col p-2">
          <img class="img-fluid" src="<?= base_url() ?>dist/img/bg_jumbotron/<?= $item->background ?>">
        </div>
      </div>
      <div class="row">
        <form action="<?= base_url() ?>jumbotron/store_bg" method="post" enctype="multipart/form-data">
          <div class="col custom-file">
            <input name="background" type="file" class="custom-file-input" id="customFile">
            <label class="custom-file-label" for="customFile">Choose file</label>
          </div>
          <div class="col pt-2 text-right">
            <button type="submit" class="btn btn-success btn-icon-split">
              <span class="icon text-white-50">
                <i class="fas fa-check"></i>
              </span>
              <span class="text">Change Background</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>