<?php if ($this->session->flashdata('message')) { ?>
  <div class="alert <?= $this->session->flashdata('alert') ?>">
    <span><?= $this->session->flashdata('message') ?></span>
  </div>
<?php } ?>
<div class="card shadow">
  <div class="card-header">
    <h4 class="m-0 font-weight-bold text-primary text-center">Insert Favorite Menu</h4>
  </div>
  <div class="card-body">
    <form action="<?= base_url() ?>favorite_item/store_insert" method="post" enctype="multipart/form-data">
      <div class="form-group">
        <label for="name">Menu Name</label>
        <input name="title" type="text" class="form-control" id="name" placeholder="Name ...">
        <small class="form-text text-danger"><?= form_error('title') ?></small>
      </div>
      <div class="form-group">
        <label for="detail">Detail Menu</label>
        <textarea name="desc" class="form-control" rows="5" id="detail"></textarea>
        <small class="form-text text-danger"><?= form_error('desc') ?></small>
      </div>
      <div class="form-group">
        <label for="detail">Background</label>
        <div class="custom-file">
          <input name="photo" type="file" class="custom-file-input" id="customFile">
          <label class="custom-file-label" for="customFile">Choose file</label>
        </div>
      </div>
      <button class="btn btn-primary" type="submit">Posting</button>
    </form>
  </div>
</div>