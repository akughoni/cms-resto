<?php if ($this->session->flashdata('message')) { ?>
  <div class="alert <?= $this->session->flashdata('alert') ?>">
    <span><?= $this->session->flashdata('message') ?></span>
  </div>
<?php } ?>
<div class="card p-2 text-center">
  <h3>Favorite Items</h3>
</div>
<div class="row mt-2">
  <?php foreach ($list_item as $key => $item) : ?>
    <div class="col-12">
      <div class="card shadow mb-4">
        <div class="card-body ">
          <img class="card-img" style="width: 25rem;" src="<?= base_url(); ?>dist/img/bg_favorite/<?= $item->bg ?>" alt="">
          <div class="card-img-overlay">
            <div class="d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary"><?= $item->title ?></h6>
              <div class="dropdown no-arrow">
                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                  <div class="dropdown-header">Actions :</div>
                  <a class="dropdown-item" href="<?= base_url(); ?>favorite_item/update_text/<?= $item->id ?>">Update Content</a>
                  <a class="dropdown-item" href="<?= base_url(); ?>favorite_item/update_bg/<?= $item->id ?>">Change Image</a>
                  <a class="dropdown-item" href="<?= base_url(); ?>favorite_item/delete_item/<?= $item->id ?>">Delete Item</a>
                </div>
              </div>
            </div>
            <p class="card-text"><?= $item->desc ?></p>
            <div class="text-center justify-content-bottom">
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endforeach ?>
</div>