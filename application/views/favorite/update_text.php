<?php if ($this->session->flashdata('message')) { ?>
  <div class="alert <?= $this->session->flashdata('alert') ?>">
    <span><?= $this->session->flashdata('message') ?></span>
  </div>
<?php } ?>
<div class="card p-2 text-center">
  <h3>Favorite Update</h3>
</div>
<div class="card mt-3 p-5">
  <form action="<?= base_url() ?>/favorite_item/store_update/<?= $item->id ?>" method="post">
    <div class="row form-group">
      <label class="col-2 form-label" for="title">Title</label>
      <div class="col-10">
        <input name="title" value="<?= $item->title ?>" class="col form-control" type="text" id="title">
        <span class="form-text text-danger"><?= form_error('title') ?></span>
      </div>
    </div>
    <div class="row form-group">
      <label class="col-2 form-label" for="desc">Descriptions</label>
      <div class="col-10">
        <textarea name="desc" class="col form-control" rows="6" id="desc"><?= $item->desc ?></textarea>
        <span class="form-text text-danger"><?= form_error('desc') ?></span>
      </div>
    </div>
    <div class="row text-right">
      <div class="col">
        <button type="submit" class="btn btn-primary btn-icon-split">
          <span class="text col">Save Changes</span>
        </button>
      </div>
    </div>
  </form>
</div>