<?php if ($this->session->flashdata('message')) { ?>
  <div class="alert <?= $this->session->flashdata('alert') ?>">
    <span><?= $this->session->flashdata('message') ?></span>
  </div>
<?php } ?>
<div class="card mt-3 p-5">
  <div class="row">
    <label class="col-12 form-label" for="title">Background Image</label>
    <div class="col p-2">
      <img class="img-fluid" src="<?= base_url() ?>dist/img/bg_favorite/<?= $item->bg ?>">
    </div>
  </div>
  <div class="row">
    <form action="<?= base_url() ?>favorite_item/store_bg/<?= $item->id ?>" method="post" enctype="multipart/form-data">
      <div class="col custom-file">
        <input name="bg" type="file" class="custom-file-input" id="customFile">
        <label class="custom-file-label" for="customFile">Choose file</label>
      </div>
      <div class="col pt-2 text-right">
        <button type="submit" class="btn btn-success btn-icon-split">
          <span class="icon text-white-50">
            <i class="fas fa-check"></i>
          </span>
          <span class="text">Change Background</span>
        </button>
      </div>
    </form>
  </div>
</div>