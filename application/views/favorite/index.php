<?php if ($this->session->flashdata('message')) { ?>
  <div class="alert <?= $this->session->flashdata('alert') ?>">
    <span><?= $this->session->flashdata('message') ?></span>
  </div>
<?php } ?>
<div class="card p-2 text-center">
  <h3>Motto Preferences</h3>
</div>
<div class="card mt-3 p-5">
  <div class="row">
    <div class="col-7">
      <form action="<?= base_url() ?>favorite/update_title" method="post">
        <div class="row form-group">
          <label class="col form-label" for="title">Descriptions</label>
          <input class="form-control" value="<?php if (isset($item->title)) {
                                                echo $item->title;
                                              } ?>" type="text" name="title" id="title">
          <span class="form-text text-danger"><?= $this->session->flashdata('error') ?></span>
        </div>
        <div class="row text-right">
          <div class="col">
            <button type="submit" class="btn btn-primary btn-icon-split">
              <span class="icon text-white-50">
                <i class="fas fa-pen"></i>
              </span>
              <span class="text col">Change Text</span>
            </button>
          </div>
        </div>
      </form>
    </div>
    <div class="offset-1 col-4">
      <div class="row">
        <label class="col-12 form-label" for="title">Background Image</label>
        <div class="col p-2">
          <?php if (isset($item->bg)) : ?>
            <img class="img-fluid" src="<?= base_url() ?>dist/img/bg_favorite/<?= $item->bg ?>">
          <?php endif; ?>
        </div>
      </div>
      <div class="row">
        <form action="<?= base_url() ?>favorite/store_bg" method="post" enctype="multipart/form-data">
          <div class="col custom-file">
            <input name="background" type="file" class="custom-file-input" id="customFile">
            <label class="custom-file-label" for="customFile">Choose file</label>
          </div>
          <div class="col pt-2 text-right">
            <button type="submit" class="btn btn-success btn-icon-split">
              <span class="icon text-white-50">
                <i class="fas fa-check"></i>
              </span>
              <span class="text">Change Background</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>