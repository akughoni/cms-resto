<?php if (isset($message)) : ?>
  <div class="alert alert-danger">
    <span><?= $message; ?></span>
  </div>
<?php endif; ?>
<div class="card shadow">
  <div class="card-header">
    <h4 class="m-0 font-weight-bold text-primary text-center">INSERT MENU</h4>
  </div>
  <div class="card-body">
    <form action="<?= base_url() ?>menu/store_insert" method="post" enctype="multipart/form-data">
      <div class="row">
        <div class="col form-group">
          <label for="name">Menu Name</label>
          <input name="name" type="text" class="form-control" id="name" placeholder="Name ...">
          <small class="form-text text-danger">Masukan Nama Makanan</small>
        </div>
        <div class="col form-group">
          <label for="Category">Category Menu</label>
          <select name="kd_cat" class="custom-select" id="Category">
            <option selected disabled>Select Category ...</option>
            <?php foreach ($category as $key => $item) : ?>
              <option value="<?= $item['id'] ?>"><?= $item['name_cat'] ?></option>
            <?php endforeach; ?>
          </select>
          <small class="form-text text-danger">Masukan Nama Makanan</small>
        </div>
      </div>
      <div class="form-group">
        <label for="detail">Detail Menu</label>
        <textarea name="detail" class="form-control" rows="5" id="detail"></textarea>
        <small class="form-text text-danger">Masukan Nama Makanan</small>
      </div>
      <div class="form-group">
        <label for="detail">Detail Menu</label>
        <div class="custom-file">
          <input name="photo" type="file" class="custom-file-input" id="customFile">
          <label class="custom-file-label" for="customFile">Choose file</label>
        </div>
      </div>
      <button type="submit">Posting</button>
    </form>
  </div>
</div>