<?php if ($this->session->flashdata('message')) { ?>
  <div class="alert <?= $this->session->flashdata('alert') ?>">
    <span><?= $this->session->flashdata('message') ?></span>
  </div>
<?php } ?>
<div class="row">
  <?php foreach ($menu_list as $key => $item) : ?>
    <div class="col-4">
      <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 font-weight-bold text-primary"><?= $item->name ?></h6>
          <div class="dropdown no-arrow">
            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
              <div class="dropdown-header">Actions :</div>
              <a class="dropdown-item" href="#">Update</a>
              <a class="dropdown-item" href="<?= base_url() ?>menu/delete_menu/<?= $item->slug ?>">Delete</a>
            </div>
          </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">
          <div class="text-center">
            <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="<?= base_url() ?>dist/user_upload/<?= $item->photo ?>" alt="">
            <?= $item->detail ?>
          </div>
        </div>
      </div>
    </div>
  <?php endforeach; ?>
</div>