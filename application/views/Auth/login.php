<body class="bg-gradient-primary">
	<div class="container">
	<div class="row justify-content-center align-middle">
	<div class="col-5 my-5">
		<?php if($message): ?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
          <p class="text-center"><?= $message; ?></p>
        </div>
    <?php endif; ?>
		<div class="card o-hidden border-0 shadow-lg">
			<div class="card-body p-0">
				<!-- Nested Row within Card Body -->
				<div class="row">
					<div class="col">
						<div class="p-5">
							<div class="text-center">
								<h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
							</div>
							<form class="user" method="post" action="<?= base_url() ?>auth/store_login">
								<div class="form-group">
									<input type="text" name="username" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Username...">
									<small class="text text-danger text-center form-text"><?= form_error('username'); ?></small>
								</div>
								<div class="form-group">
									<input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
									<small class="text text-danger text-center form-text"><?= form_error('password'); ?></small>
								</div>
								<button type="submit" class="btn btn-primary btn-user btn-block">
									Login
								</button>
								<hr>
							</form>
							<hr>
							<div class="text-center">
								<a class="small" href="forgot-password.html">Forgot Password?</a>
							</div>
							<div class="text-center">
								<a class="small" href="<?= base_url() ?>auth/auth_register">Create an Account!</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
<!-- </body> -->