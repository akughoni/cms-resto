<body class="bg-gradient-primary">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-5 my-5">
        <?php if($message): ?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
          <p class="text-center"><?= $message; ?></p>
        </div>
        <?php endif; ?>
        <div class="card o-hidden border-0 shadow-lg">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                  </div>
                  <form class="user" action="<?= base_url() ?>auth/store_register" method="POST">
                    <div class="form-group">
                      <input type="text" name="username" class="form-control form-control-user" id="exampleFirstName" placeholder="User Name">
                      <small class="form-text text-danger text-center"><?= form_error('username') ?></small>
                    </div>
                    <div class="form-group">
                      <input type="email" name="email" class="form-control form-control-user" id="exampleInputEmail" placeholder="Email Address">
                      <small class="form-text text-danger text-center"><?= form_error('email') ?></small>
                    </div>
                    <div class="form-group">
                      <input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                      <small class="form-text text-danger text-center"><?= form_error('password') ?></small>
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                      Register Account
                    </button>
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="forgot-password.html">Forgot Password?</a>
                  </div>
                  <div class="text-center">
                    <a class="small" href="<?= base_url() ?>auth/auth_login">Already have an account? Login!</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- </body> -->